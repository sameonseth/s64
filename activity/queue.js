let collection = [];


// Write the queue functions below.


function print() {
    return collection;
}


function enqueue(new_item){
    collection[size()] = new_item;
    return collection;
}


function dequeue(){
    let temp_items = [];

    for(let index = 0; index < size()-1; index++){
        temp_items[index] = collection[index+1];
    }

    collection = temp_items;
    return collection;
}


function front(){
    return collection[0];
}


function size(){
    return collection.length;
}


function isEmpty() {
    return size() === 0;
}


module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};